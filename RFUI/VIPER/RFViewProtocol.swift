//
//  RFViewProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/13/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import Swinject
import AsyncDisplayKit
import RFExtensions

public protocol RFViewProtocol: InjectableView {
    associatedtype MainNode: RFMainNode
    
    var modules: Container { get }
    
    var navigationController: UINavigationController? { get }
    var tabBarController: UITabBarController? { get }
    
    var navigationItem: UINavigationItem { get }
    
    var view: UIView! { get }
    
    var node: MainNode! { get }
    
    var title: String? { get set }
    
    var statusBarStyle: UIStatusBarStyle { get set }

    var isNavigationBarShowing: Bool? { get set }
    
    var navigationBackButton: UIBarButtonItem? { get set }
    
    func dismiss(animated flag: Bool, completion: (() -> Void)?)
}

public extension RFViewProtocol {
    var isNavigationBarShowing: Bool? {
        get {
            guard let isHidden = navigationController?.isNavigationBarHidden else { return nil }
            
            return !isHidden
        }
        
        set {
            guard let showing = newValue else { return }
            
            navigationController?.setNavigationBarHidden(!showing, animated: true)
        }
    }
    
    var navigationBackButton: UIBarButtonItem? {
        get {
            navigationItem.backBarButtonItem
        }
        
        set {
            navigationItem.backBarButtonItem = newValue
        }
    }
}
