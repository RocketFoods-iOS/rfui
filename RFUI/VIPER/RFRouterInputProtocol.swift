//
//  RFRouterInputProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 3/12/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import RFExtensions

public protocol RFRouterInputProtocol: SignDelegate {
    var navigationController: UINavigationController? { get set }
    var tabBarController: UITabBarController? { get set }
    
    func signChoose()
    
    func signIn(completion: ((Bool) -> Void)?)
    
    func signOut()
}

public extension RFRouterInputProtocol {
    func signChoose() {
        if let signDelegate = tabBarController as? SignDelegate {
            signDelegate.signChoose()
        } else if let signDelegate = navigationController?.tabBarController as? SignDelegate {
            signDelegate.signChoose()
        }
    }
    
    func signIn(completion: ((Bool) -> Void)? = nil) {
        if let signDelegate = tabBarController as? SignDelegate {
            signDelegate.signIn(completion: completion)
        } else if let signDelegate = navigationController?.tabBarController as? SignDelegate {
            signDelegate.signIn(completion: completion)
        }
    }
    
    func signOut() {
        if let signDelegate = tabBarController as? SignDelegate {
            signDelegate.signOut()
        } else if let signDelegate = navigationController?.tabBarController as? SignDelegate {
            signDelegate.signOut()
        }
    }
}
