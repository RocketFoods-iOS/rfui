//
//  RFViewInputProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 3/12/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import Swinject

public protocol RFViewInputProtocol: class {
    associatedtype MainNode: RFMainNode
    
    var navigationController: UINavigationController? { get }
    var tabBarController: UITabBarController? { get }
    
    var node: MainNode! { get }

    var title: String? { get set }
    
    var statusBarStyle: UIStatusBarStyle { get set }

    var isNavigationBarShowing: Bool? { get set }
    
    var navigationBackButton: UIBarButtonItem? { get set }

    var modules: Container { get }

    func dismiss(animated flag: Bool, completion: (() -> Void)?)
}
