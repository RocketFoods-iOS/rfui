//
//  RFViewOutputProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 3/12/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol RFViewOutputProtocol: class {
    func didLoad()

    func willAppear(_ animated: Bool)

    func willDisappear(_ animated: Bool)
}
