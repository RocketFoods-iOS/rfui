//
//  RFPresenterInputProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 3/12/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol RFPresenterInputProtocol: class {
    func configureView()

    func showView()

    func hideView()
}
