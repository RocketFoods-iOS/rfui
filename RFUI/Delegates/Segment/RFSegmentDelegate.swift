//
//  RFSegmentDelegate.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 30.08.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Foundation

public protocol RFSegmentDelegate: AnyObject {
    func segmentNode(_ segmentNode: RFSegmentNode, selectedIndexDidChanged selectedIndex: Int)
}
