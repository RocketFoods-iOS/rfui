//
//  RFKeyboardDelegate.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 28/02/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFKeyboardDelegate: AnyObject {
    associatedtype ViewInput: RFViewInputProtocol
    
    var view: ViewInput! { get }
    
    var keyboardObserver: (NSObjectProtocol, NSObjectProtocol)? { get set }
    
    func keyboardDidChange()
}

extension RFKeyboardDelegate {
    public func keyboardSubscribe() {
        guard view != nil else { return }
        
        keyboardObserver = RFKeyboard.subscribe(
            view,
            willShow: keyboardWillShow(notification:),
            willHide: keyboardWillHide(notification:)
        )
    }
    
    public func keyboardUnsubscribe() {
        RFKeyboard.unsubscribe(keyboardObserver)
    }
    
    public func keyboardWillShow(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }

        guard let animationTime =
            (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue else { return }

        guard let height =
            (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height else { return }

        RFKeyboard.isShown = true
        RFKeyboard.animationTime = animationTime
        RFKeyboard.height = height

        keyboardDidChange()
    }

    public func keyboardWillHide(notification: Notification) {
        guard let userInfo = notification.userInfo else { return }

        guard let animationTime =
            (userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue else { return }

        guard let height =
            (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue.height else { return }

        RFKeyboard.isShown = false
        RFKeyboard.animationTime = animationTime
        RFKeyboard.height = height

        keyboardDidChange()
    }
    
    public func keyboardDidChange() {
        guard view != nil else { return }
        
        view.node.updateKeyboardNode()
    }
}

open class RFKeyboard: UIResponder {
    public static var isShown = false

    public static var animationTime = Double(0)
    public static var height = CGFloat(0)

    public static func subscribe(_ view: Any?, willShow: @escaping (Notification) -> Void,
                                 willHide: @escaping (Notification) -> Void) -> (NSObjectProtocol, NSObjectProtocol)? {
        guard let view = (view as? ASDKViewController<ASDisplayNode>)?.view else { return nil }

        view.addGestureRecognizer(UITapGestureRecognizer { view.endEditing(false) })

        let willShowObserver = NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillShowNotification,
            object: nil,
            queue: nil,
            using: willShow
        )

        let willHideObserver = NotificationCenter.default.addObserver(
            forName: UIResponder.keyboardWillHideNotification,
            object: nil,
            queue: nil,
            using: willHide
        )

        return (willShowObserver, willHideObserver)
    }

    public static func unsubscribe(_ observer: (NSObjectProtocol, NSObjectProtocol)?) {
        guard let observer = observer else { return }

        NotificationCenter.default.removeObserver(observer.0)
        NotificationCenter.default.removeObserver(observer.1)
    }
}
