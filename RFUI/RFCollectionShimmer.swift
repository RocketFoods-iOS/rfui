//
//  RFCollectionShimmer.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 6/29/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFCollectionShimmer {
    var isShimmering: Bool { get set }
    
    func shimmerNumberOfItems() -> Int
    
    func shimmer(nodeBlockForItemAt indexPath: IndexPath) -> ASCellNodeBlock
}
