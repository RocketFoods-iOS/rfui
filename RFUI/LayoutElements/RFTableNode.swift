//
//  RFTableNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 17.06.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import PromiseKit
import ListDiff
import AsyncDisplayKit

open class RFTableNode: ASTableNode, RFElementProtocol,
                        RFElementLayoutSpecsProtocol, RFShimmerProtocol, UIScrollViewDelegate {
    
    open var insets = UIEdgeInsets.zero
    
    open var isShimmering = false {
        didSet {
            isUserInteractionEnabled = !isShimmering
        }
    }
    
    open lazy var shimmerAnimation: CAKeyframeAnimation = {
        defaultShimmerAnimation
    }()
    
    open lazy var shimmerLayer: CAGradientLayer = {
        defaultShimmerLayer
    }()
    
    open var shimmeringNode: ASDisplayNode { self }
    
    open func modelsHashes(atSection section: Int = 0) -> [Int?] {
        guard section < numberOfSections,
              let numberOfRows = dataSource?.tableNode?(self, numberOfRowsInSection: section) else {
            return []
        }
        
        return (0 ..< numberOfRows).map { item -> Int? in
            let indexPath = IndexPath(item: item, section: section)
            
            guard let node = nodeForRow(at: indexPath) as? RFCellNode else {
                return nil
            }
            
            return node.modelHash
        }
    }

    public override init(style: UITableView.Style) {
        super.init(style: style)
        
        view.delaysContentTouches = false
    }
    
    open func reloadData(animated: Bool = false, completion: (() -> Void)? = nil) {
        if animated {
            performBatch(animated: true) { [weak self] in
                let sections = IndexSet(0..<(self?.numberOfSections ?? 0))
                
                self?.reloadSections(sections, with: .fade)
            } completion: { _ in
                completion?()
            }
        } else {
            reloadData(completion: completion)
        }
    }

    open var automaticallyHideKeyboardOnScroll = true
    
    // MARK: - Scroll

    open func scrollViewDidScroll(_: UIScrollView) {
        guard automaticallyHideKeyboardOnScroll else { return }

        view.endEditing(false)
    }
    
    open func moveRows(at indexPaths: [(IndexPath, IndexPath)]) {
        indexPaths.forEach { [moveRow] in
            moveRow($0.0, $0.1)
        }
    }
    
    // MARK: - Differable
    
    open func performDifferableUpdates(_ difference: List.Result, in section: Int? = 0,
                                       with animation: UITableView.RowAnimation = .automatic) -> Promise<Bool> {
        Promise { seal in
            guard difference.hasChanges || section == nil else { throw PMKError.cancelled }
            
            let difference = difference.forBatchUpdates()
            
            performBatchUpdates({ [weak self] in
                
                if let section = section {
                    
                    if numberOfSections <= section {
                        let sectionInserts = IndexSet(integersIn: numberOfSections ... section)
                        
                        self?.insertSections(sectionInserts, with: animation)
                    }
                    
                } else if numberOfSections == 0 {
                    
                    self?.insertSections(IndexSet(integer: 0), with: animation)
                    
                } else if numberOfSections > 0 {
                    
                    let sectionDeletes = IndexSet(integersIn: 1 ..< numberOfSections)
                    
                    self?.deleteSections(sectionDeletes, with: animation)
                    
                }
                
                let section = section ?? 0

                self?.deleteRows(at: difference.deletes(for: section), with: animation)
                self?.insertRows(at: difference.inserts(for: section), with: animation)
                self?.moveRows(at: difference.moves(for: section))
            }) {
                seal.fulfill($0)
            }
        }
    }
}

public extension RFShimmerProtocol where Self: ASTableNode {
    func updateShimmering() {
        updateShimmering(completion: nil)
    }
    
    func updateShimmering(completion: (() -> Void)?) {
        reloadData(completion: completion)
    }
    
    func updateSubnodesShimmering() {
        updateShimmering()
    }
}
