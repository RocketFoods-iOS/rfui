//
//  RFSegmentNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 11.08.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFSegmentNode: RFDisplayNode {
    public let segmentNode: RFDisplayNode
    
    open var segmentView: UISegmentedControl? {
        segmentNode.view as? UISegmentedControl
    }
    
    @available (iOS 13, *)
    open var selectedImageView: UIImageView? {
        segmentView?.subviews.first(where: {
            ($0 as? UIImageView)?.subviews.isEmpty ?? false
        }) as? UIImageView
    }
    
    open var initialSelectedImageOffsetX: CGFloat?

    public var selectedSegmentIndex: Int {
        get {
            guard let segmentView = segmentNode.view as? UISegmentedControl else { return 0 }

            return segmentView.selectedSegmentIndex
        }

        set {
            segmentView?.selectedSegmentIndex = newValue
        }
    }

    open override var tintColor: UIColor? {
        get {
            guard let segmentView = segmentNode.view as? UISegmentedControl else { return .clear }

            return segmentView.tintColor
        }

        set {
            guard let segmentView = segmentNode.view as? UISegmentedControl else { return }

            segmentView.tintColor = newValue
        }
    }

    public weak var delegate: RFSegmentDelegate?

    public init(_ items: [String]) {
        segmentNode = RFDisplayNode(viewBlock: { () -> UIView in

            let segmentView = UISegmentedControl(items: items)

            return segmentView

        }, didLoad: { node in

            guard let segmentView = node.view as? UISegmentedControl else { return }

            segmentView.tintColor = .blue
            segmentView.selectedSegmentIndex = 0

        })

        super.init()

        if let segmentView = segmentNode.view as? UISegmentedControl {
            segmentView.addTarget(for: .valueChanged) { [weak self] in

                guard let self = self else { return }

                self.delegate?.segmentNode(self, selectedIndexDidChanged: self.selectedSegmentIndex)
            }
        }
    }

    open override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        return segmentNode.centerSpec
    }
}
