//
//  RFCellNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFCellNode: ASCellNode, RFElementProtocol, RFElementLayoutSpecsProtocol, RFShimmerProtocol {
    open var insets = UIEdgeInsets.zero
    
    // MARK: - Shimmer
    
    open var isShimmering = false {
        didSet {
            isUserInteractionEnabled = !isShimmering
        }
    }
    
    open lazy var shimmerAnimation: CAKeyframeAnimation = {
        defaultShimmerAnimation
    }()
    
    open lazy var shimmerLayer: CAGradientLayer = {
        defaultShimmerLayer
    }()
    
    open var shimmeringNode: ASDisplayNode { self }
    
    open func updateShimmering() {
        defaultUpdateShimmering()
    }
    
    open func updateSubnodesShimmering() {
        defaultUpdateSubnodesShimmering()
    }
    
    // MARK: - Loader
    
    open var isLoading: Bool = false {
        willSet {
            if isLoading != newValue {
                setLoadingState(newValue)
            }
        }
    }
    
    open var loaderNode: RFLoaderNode?
    
    open var modelHash: Int?
    
    public override init() {
        super.init()
        
        automaticallyManagesSubnodes = true
        
        addActions()
    }
    
    open override func didLoad() {
        super.didLoad()
        
        updateShimmering()
        
        loaderNode = {
            let node = RFLoaderNode(style: .gray)
            
            node.stopAnimating()

            return node
        }()
    }
    
    open override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        let superLayoutSpec = super.layoutSpecThatFits(constrainedSize)
        
        guard let loaderNode = loaderNode else {
            return ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: .minimumXY, child: superLayoutSpec)
        }
        
        let imageTitleLoaderSpec = ASBackgroundLayoutSpec(child: superLayoutSpec, background: loaderNode.centerSpec)

        return ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: .minimumXY, child: imageTitleLoaderSpec)
    }
    
    open func addActions() { }
}

fileprivate extension RFCellNode {
    func setLoadingState(_ isLoading: Bool) {
        isUserInteractionEnabled = !isLoading
        
        let touchAnimationDuration = 0.24
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = touchAnimationDuration
            
            self.loaderNode?.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            
            if isLoading {
                self.loaderNode?.startAnimating()
            } else {
                self.loaderNode?.stopAnimating()
            }
        }
    }
}
