//
//  RFEmbeddedSwitchNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 07.06.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFEmbeddedSwitchNode: RFDisplayNode {
    public let switchNode: RFDisplayNode

    public var isOn: Bool {
        get {
            guard let switchView = switchNode.view as? UISwitch else { return false }

            return switchView.isOn
        }

        set {
            guard let switchView = switchNode.view as? UISwitch else { return }

            guard Thread.isMainThread else {
                return DispatchQueue.main.async { switchView.isOn = newValue }
            }
            
            switchView.isOn = newValue
        }
    }

    public var onTintColor: UIColor? {
        get {
            guard let switchView = switchNode.view as? UISwitch else { return .clear }

            return switchView.onTintColor
        }

        set {
            guard let switchView = switchNode.view as? UISwitch else { return }

            switchView.onTintColor = newValue
        }
    }

    open override var tintColor: UIColor? {
        get {
            guard let switchView = switchNode.view as? UISwitch else { return .clear }

            return switchView.tintColor
        }

        set {
            guard let switchView = switchNode.view as? UISwitch else { return }

            switchView.tintColor = newValue
        }
    }

    public init(size: ASLayoutSize) {
        switchNode = RFDisplayNode(viewBlock: { () -> UIView in

            let switchSize = CGSize(
                width: size.width.value,
                height: size.height.value
            )

            let switchView = UISwitch(frame: CGRect(origin: .zero, size: switchSize))

            return switchView

        }, didLoad: { node in

            guard let switchView = node.view as? UISwitch else { return }

            switchView.onTintColor = .blue
            switchView.tintColor = UIColor(0, 0, 0, 0.1)

            switchView.isOn = false

        })

        super.init()
    }

    open override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        switchNode.centerSpec
    }
}
