//
//  RFAnimationProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 25/05/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import AsyncDisplayKit

public protocol RFAnimationProtocol {
    func animateLayoutTransition(_ context: ASContextTransitioning)
    
    func animate(with context: ASContextTransitioning)
}
