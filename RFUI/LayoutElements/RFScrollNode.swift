//
//  RFScrollNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 25.07.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFScrollNode: ASScrollNode, RFElementProtocol, RFElementLayoutSpecsProtocol, UIScrollViewDelegate {
    open var insets = UIEdgeInsets.zero

    open var automaticallyHideKeyboardOnDrag: Bool {
        get {
            view.keyboardDismissMode != .none
        }
        
        set {
            view.keyboardDismissMode = newValue ? .onDrag : .none
        }
    }
    
    public override init() {
        super.init()
        
        style.flexShrink = 1.0
        
        automaticallyManagesSubnodes = true
        automaticallyManagesContentSize = true
        
        automaticallyHideKeyboardOnDrag = true
        
        view.delegate = self
        view.alwaysBounceVertical = true
        view.delaysContentTouches = false
    }
    
    open func scrollToVisibleNode(_ node: RFElementProtocol & RFElementLayoutSpecsProtocol, offset: CGFloat? = nil, animated flag: Bool) {
        var frame = node.frame.inset(by: node.insets.inverse)
        
        frame.origin.x = 0
        
        if let offset = offset {
            frame.origin.y += offset
        }
        
        DispatchQueue.main.async { [weak self] in
            if let height = self?.frame.size.height,
                let contentHeight = self?.view.contentSize.height {
                let maxOffset = contentHeight - height
                
                frame.origin.y = min(frame.origin.y, maxOffset)
            }
            
            self?.view.setContentOffset(frame.origin, animated: flag)
//            self?.view.scrollRectToVisible(frame, animated: flag)
        }
    }
}
