//
//  RFFocusProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 4/10/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFFocusProtocol {
    var keyboardFocus: (RFMainNode?, RFScrollNode?, (RFElementProtocol & RFElementLayoutSpecsProtocol)?)? { get set }
}
