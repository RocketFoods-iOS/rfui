//
//  RFShimmerProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 6/27/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFShimmerProtocol: class {
    var isShimmering: Bool { get set }
    
    var shimmerAnimation: CAKeyframeAnimation { get set }
    
    var shimmerLayer: CAGradientLayer { get set }
    
    var shimmeringNode: ASDisplayNode { get }
    
    func updateShimmering()
    
    func updateSubnodesShimmering()
}

public extension RFShimmerProtocol where Self: ASDisplayNode {
    var defaultShimmerAnimation: CAKeyframeAnimation {
        let locationsAnimation = CAKeyframeAnimation(keyPath: #keyPath(CAGradientLayer.locations))
        
        locationsAnimation.duration = 1.5
        locationsAnimation.repeatCount = .infinity
        
        locationsAnimation.values = [
            [-1, -0.55, -0.45, 0],
            [-1, -0.55, -0.45, 0],
            [0, 0.45, 0.55, 1],
            [1, 1.45, 1.55, 2],
            [1, 1.45, 1.55, 2],
        ]
        
        locationsAnimation.keyTimes = [0, 0.2, 0.5, 0.8, 1]
        
        return locationsAnimation
    }
    
    var defaultShimmerLayer: CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        
        let calculatedRect = CGRect(origin: .zero, size: calculatedSize)
        
        let increasedInsets = UIEdgeInsets(
            top: -1,
            left: -1,
            bottom: -3,
            right: -3
        )
        
        let increasedRect = calculatedRect.inset(by: increasedInsets)
        
        gradientLayer.frame = increasedRect
        
        gradientLayer.colors = [
            UIColor.mainBorderLight.cgColor,
            UIColor.white.cgColor,
            UIColor.white.cgColor,
            UIColor.mainBorderLight.cgColor,
        ]
        
        gradientLayer.locations = [-1, -0.55, -0.45, 0]
        
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 1)
        
        let cornersLayer = CAShapeLayer()
        
        let cornersPath = UIBezierPath(
            roundedRect: increasedRect,
            cornerRadius: cornerRadius
        )
        
        cornersLayer.path = cornersPath.cgPath
        
        gradientLayer.mask = cornersLayer
        
        return gradientLayer
    }
    
    func startShimmering(animated: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            self.isShimmering = true
            
            self.shimmerLayer.opacity = 0
            
            if animated {
                CATransaction.begin()
            }
            
            self.shimmeringNode.layer.addSublayer(self.shimmerLayer)
            
            self.shimmerLayer.add(self.shimmerAnimation, forKey: nil)
            
            self.shimmerLayer.opacity = 1
            
            if animated {
                CATransaction.commit()
            }
        }
    }
    
    func stopShimmering(animated: Bool = true) {
        DispatchQueue.main.async { [weak self] in
            self?.isShimmering = false
            
            let animationCompletion: (() -> Void) = {
                self?.shimmerLayer.removeAllAnimations()
                self?.shimmerLayer.removeFromSuperlayer()
            }
            
            if animated {
                CATransaction.begin()
                
                CATransaction.setCompletionBlock(animationCompletion)
            }
            
            self?.shimmerLayer.opacity = 0
            
            if animated {
                CATransaction.commit()
            } else {
                animationCompletion()
            }
        }
    }
    
    func defaultUpdateShimmering() {
        if isShimmering {
            startShimmering()
        } else {
            stopShimmering()
        }
    }
    
    func defaultUpdateSubnodesShimmering() {
        updateShimmering()
        
        subnodes?.forEach {
            guard let shimmerNode = $0 as? RFShimmerProtocol & ASDisplayNode else { return }
            
            shimmerNode.updateSubnodesShimmering()
        }
    }
}
