//
//  RFDisplayNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFDisplayNode: ASDisplayNode, RFElementProtocol, RFElementLayoutSpecsProtocol, RFShimmerProtocol {
    open var insets = UIEdgeInsets.zero
    
    // MARK: - Shimmer
    
    open var isShimmering = false {
        didSet {
            isUserInteractionEnabled = !isShimmering
        }
    }
    
    open lazy var shimmerAnimation: CAKeyframeAnimation = {
        defaultShimmerAnimation
    }()
    
    open lazy var shimmerLayer: CAGradientLayer = {
        defaultShimmerLayer
    }()
    
    open var shimmeringNode: ASDisplayNode { self }
    
    open func updateShimmering() {
        defaultUpdateShimmering()
    }
    
    open func updateSubnodesShimmering() {
        defaultUpdateSubnodesShimmering()
    }
    
    public override init() {
        super.init()
        
        automaticallyManagesSubnodes = true
    }
    
    public convenience init(viewBlock: @escaping ASDisplayNodeViewBlock,
                            didLoad didLoadBlock: ASDisplayNodeDidLoadBlock? = nil) {
        self.init()
        
        setViewBlock(viewBlock)
        
        guard let didLoadBlock = didLoadBlock else { return }
        
        onDidLoad(didLoadBlock)
    }
    
    public convenience init(layerBlock: @escaping ASDisplayNodeLayerBlock,
                            didLoad didLoadBlock: ASDisplayNodeDidLoadBlock? = nil) {
        self.init()
        
        setLayerBlock(layerBlock)
        
        guard let didLoadBlock = didLoadBlock else { return }
        
        onDidLoad(didLoadBlock)
    }
    
    open override func didLoad() {
        super.didLoad()
        
        updateShimmering()
    }
}

public extension ASDisplayNode {
    func transitionLayout() {
        transitionLayout(withAnimation: true,
                         shouldMeasureAsync: true, measurementCompletion: nil)
    }
}
