//
//  RFMainNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 09/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFMainNode: RFDisplayNode {
    
    // MARK: - Status Bar
    
    open lazy var statusBarNode: RFDisplayNode = {
        let node = RFDisplayNode()
        
        node.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimensionAuto
        )
        
        node.zPosition = 1000
        
        return node
    }()
    
    open var statusBarBackground: UIColor? = nil {
        willSet {
            transitionLayout(withAnimation: true, shouldMeasureAsync: true)
        }
        
        didSet {
            closestViewController?.setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    open func updateStatusBarNode() {
        statusBarNode.size.height = ASDimension(unit: .points, value: layoutMargins.top)
        
        statusBarNode.setNeedsLayout()
    }
    
    // MARK: - Keyboard
    
    open var keyboardNode: RFDisplayNode = {
        let node = RFDisplayNode()
        
        node.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimensionAuto
        )
        
        return node
    }()
    
    open var keyboardFocus: (RFScrollNode, RFElementProtocol & RFElementLayoutSpecsProtocol)? {
        didSet {
            scrollToFocusedNode()
        }
    }
    
    open func updateKeyboardNode() {
        let tabBarHeight = closestViewController?.tabBarController?.tabBar.frame.height ?? 0
        let height = RFKeyboard.isShown ? RFKeyboard.height - tabBarHeight : 0
        
        keyboardNode.size.height = ASDimension(unit: .points, value: height)
        
        keyboardNode.setNeedsLayout()
        keyboardNode.layoutIfNeeded()
        
        setNeedsLayout()
        layoutIfNeeded()
        
        scrollToFocusedNode()
    }
    
    // MARK: - Bottom Safe Area
    
    open lazy var bottomSafeAreaNode: RFDisplayNode = {
        let node = RFDisplayNode()
        
        node.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimensionAuto
        )
        
        return node
    }()
    
    open func updateBottomSafeAreaNode() {
        bottomSafeAreaNode.size.height = ASDimension(unit: .points, value: layoutMargins.bottom)
        
        bottomSafeAreaNode.setNeedsLayout()
    }
    
    public required override init() {
        super.init()
        
        backgroundColor = .white
        
        insetsLayoutMarginsFromSafeArea = true
        automaticallyRelayoutOnLayoutMarginsChanges = true
    }
    
    open override func layoutMarginsDidChange() {
        updateStatusBarNode()
        updateBottomSafeAreaNode()
        
        super.layoutMarginsDidChange()
    }
    
    open override func animateLayoutTransition(_ context: ASContextTransitioning) {
        UIView.animate(withDuration: 0.24) { [weak self] in
            self?.statusBarNode.backgroundColor = self?.statusBarBackground
        }
    }
    
    func scrollToFocusedNode() {
        let keyboardHeight = keyboardNode.size.height.value
        
        guard keyboardHeight != 0,
            let (scrollNode, focusNode) = keyboardFocus else { return }

        scrollNode.scrollToVisibleNode(focusNode, animated: true)
    }
    
    open func addActions() { }
}
