//
//  RFCollectionNode+VFlowLayoutSize.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 21.04.2021.
//  Copyright © 2021 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension RFCollectionNode {
    func calculateSizeThatFitsVFlowLayout(_ constrainedSize: CGSize,
                                          flowLayout: UICollectionViewFlowLayout?) -> CGSize {
        let constrainedWidth = constrainedSize.width
            - contentInset.left - contentInset.right
        
        var lines = numberOfItems(inSection: 0) > 0 ? 1 : 0
        var width = constrainedWidth
        
        let verticalInset = contentInset.top + contentInset.bottom
        let spacingHeight = flowLayout?.minimumLineSpacing ?? 0
        
        var height = verticalInset
        
        var maxHeight = CGFloat(0)
        
        for i in 0 ..< numberOfItems(inSection: 0) {
            if width <= 0 {
                if lines > 0 {
                    height += spacingHeight
                }
                
                lines += 1
                height += maxHeight
                maxHeight = 0
                
                width = constrainedWidth
            }
            
            if i > 0, let spacing = flowLayout?.minimumInteritemSpacing {
                width -= spacing
            }
            
            let currentNode = nodeForItem(at: IndexPath(item: i, section: 0))
            
            width -= currentNode?.calculatedSize.width ?? 0
            
            maxHeight = max(maxHeight, currentNode?.calculatedSize.height ?? 0)
        }
        
        height += maxHeight
        
        let size = CGSize(
            width: constrainedSize.width,
            height: height
        )
        
        return size
    }
}
