//
//  RFCollectionNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import PromiseKit
import ListDiff
import AsyncDisplayKit

open class RFCollectionNode: ASCollectionNode, RFElementProtocol,
                             RFElementLayoutSpecsProtocol, RFShimmerProtocol, UIScrollViewDelegate {
    
    open var insets = UIEdgeInsets.zero
    
    open var isShimmering = false {
        didSet {
            isUserInteractionEnabled = !isShimmering
        }
    }
    
    open lazy var shimmerAnimation: CAKeyframeAnimation = {
        defaultShimmerAnimation
    }()
    
    open lazy var shimmerLayer: CAGradientLayer = {
        defaultShimmerLayer
    }()
    
    open var shimmeringNode: ASDisplayNode { self }
    
    open func modelsHashes(atSection section: Int = 0) -> [Int?] {
        guard section < numberOfSections,
              let numberOfRows = dataSource?.collectionNode?(self,
                                                             numberOfItemsInSection: section) else {
            return []
        }
        
        return (0 ..< numberOfRows).map { item -> Int? in
            let indexPath = IndexPath(item: item, section: section)
            
            guard let node = nodeForItem(at: indexPath) as? RFCellNode else {
                return nil
            }
            
            return node.modelHash
        }
    }

    public required override init(collectionViewLayout layout: UICollectionViewLayout) {
        super.init(collectionViewLayout: layout)
        
        view.delaysContentTouches = false
    }

    public override init(frame: CGRect, collectionViewLayout layout: UICollectionViewLayout) {
        super.init(frame: frame, collectionViewLayout: layout)
        
        view.delaysContentTouches = false
    }

    public override init(frame: CGRect, collectionViewLayout: UICollectionViewLayout,
                         layoutFacilitator: ASCollectionViewLayoutFacilitatorProtocol?) {
        super.init(frame: frame, collectionViewLayout: collectionViewLayout, layoutFacilitator: layoutFacilitator)
        
        view.delaysContentTouches = false
    }
    
    open func reloadData(animated: Bool = false, completion: (() -> Void)? = nil) {
        if animated {
            performBatch(animated: true) { [weak self] in
                let sections = IndexSet(0..<(self?.numberOfSections ?? 0))

                self?.reloadSections(sections)
            } completion: { _ in
                completion?()
            }
        } else {
            reloadData(completion: completion)
        }
    }

    open var automaticallyHideKeyboardOnScroll = true

    open func scrollViewDidScroll(_: UIScrollView) {
        guard automaticallyHideKeyboardOnScroll else { return }

        view.endEditing(false)
    }
    
    open func moveItems(at indexPaths: [(IndexPath, IndexPath)]) {
        indexPaths.forEach { [moveItem] in
            moveItem($0.0, $0.1)
        }
    }
    
    // MARK: - Differable
    
    open func performDifferableUpdates(_ difference: List.Result, in section: Int? = 0) -> Promise<Bool> {
        Promise { seal in
            guard difference.hasChanges || section == nil else { throw PMKError.cancelled }
            
            let difference = difference.forBatchUpdates()
            
            performBatchUpdates({ [weak self] in
                if let section = section {
                    
                    if numberOfSections <= section {
                        let sectionInserts = IndexSet(integersIn: numberOfSections ... section)
                        
                        self?.insertSections(sectionInserts)
                    }
                    
                } else if numberOfSections == 0 {
                    
                    self?.insertSections(IndexSet(integer: 0))
                    
                } else if numberOfSections > 0 {
                    
                    let sectionDeletes = IndexSet(integersIn: 1 ..< numberOfSections)
                    
                    self?.deleteSections(sectionDeletes)
                    
                }
                
                let section = section ?? 0
                
                self?.deleteItems(at: difference.deletes(for: section))
                self?.insertItems(at: difference.inserts(for: section))
                self?.moveItems(at: difference.moves(for: section))
            }) {
                seal.fulfill($0)
            }
        }
    }
}

public extension RFShimmerProtocol where Self: ASCollectionNode {
    func updateShimmering() {
        updateShimmering(completion: nil)
    }
    
    func updateShimmering(completion: (() -> Void)?) {
        reloadData(completion: completion)
    }
    
    func updateSubnodesShimmering() {
        updateShimmering()
    }
}
