//
//  RFCollectionNode+Differable.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 28.05.2021.
//  Copyright © 2021 Nikita Arutyunov. All rights reserved.
//

import PromiseKit
import DifferenceKit

public extension RFCollectionNode {
    /// Applies multiple animated updates in stages using `StagedChangeset`.
    ///
    /// - Note: There are combination of changes that crash when applied simultaneously in `performBatchUpdates`.
    ///         Assumes that `StagedChangeset` has a minimum staged changesets to avoid it.
    ///         The data of the data-source needs to be updated synchronously before `performBatchUpdates` in every stages.
    ///
    /// - Parameters:
    ///   - stagedChangeset: A staged set of changes.
    ///   - interrupt: A closure that takes an changeset as its argument and returns `true` if the animated
    ///                updates should be stopped and performed reloadData. Default is nil.
    func reload<C>(using stagedChangeset: StagedChangeset<C>,
                   interrupt: ((Changeset<C>) -> Bool)? = nil) -> Promise<Bool> {
        let (promise, seal) = Promise<Bool>.pending()
        
        guard stagedChangeset.count > 0 else {
            seal.fulfill(true)
            
            return promise
        }

        for changeset in stagedChangeset {
            if let interrupt = interrupt, interrupt(changeset) {
                reloadData()
                
                seal.reject(NSError(domain: "me.nickaroot.rfui", code: 1, userInfo: nil))
                
                return promise
            }

            performBatchUpdates {
                if !changeset.sectionDeleted.isEmpty {
                    deleteSections(IndexSet(changeset.sectionDeleted))
                }

                if !changeset.sectionInserted.isEmpty {
                    insertSections(IndexSet(changeset.sectionInserted))
                }

                if !changeset.sectionUpdated.isEmpty {
                    reloadSections(IndexSet(changeset.sectionUpdated))
                }

                for (source, target) in changeset.sectionMoved {
                    moveSection(source, toSection: target)
                }

                if !changeset.elementDeleted.isEmpty {
                    deleteItems(at: changeset.elementDeleted.map { IndexPath(item: $0.element, section: $0.section) })
                }

                if !changeset.elementInserted.isEmpty {
                    insertItems(at: changeset.elementInserted.map { IndexPath(item: $0.element, section: $0.section) })
                }

                if !changeset.elementUpdated.isEmpty {
                    reloadItems(at: changeset.elementUpdated.map { IndexPath(item: $0.element, section: $0.section) })
                }

                for (source, target) in changeset.elementMoved {
                    moveItem(at: IndexPath(item: source.element, section: source.section), to: IndexPath(item: target.element, section: target.section))
                }
            } completion: { isComplete in
                seal.fulfill(isComplete)
            }
        }
        
        return promise
    }
}
