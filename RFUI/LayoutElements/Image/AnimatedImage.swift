//
//  AnimatedImage.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/27/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public func animatedImage(with data: Data) -> ASAnimatedImageProtocol? {
    ASPINRemoteImageDownloader().animatedImage(with: data)
}
