//
//  RFImageNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFNetworkImageNode: ASNetworkImageNode, RFElementProtocol, RFElementLayoutSpecsProtocol, RFShimmerProtocol {
    open var insets = UIEdgeInsets.zero
    
    // MARK: - Shimmer
    
    open var isShimmering = false {
        didSet {
            isUserInteractionEnabled = !isShimmering
        }
    }
    
    open lazy var shimmerAnimation: CAKeyframeAnimation = {
        defaultShimmerAnimation
    }()
    
    open lazy var shimmerLayer: CAGradientLayer = {
        defaultShimmerLayer
    }()
    
    open var shimmeringNode: ASDisplayNode { self }
    
    open func updateShimmering() {
        defaultUpdateShimmering()
    }
    
    open func updateSubnodesShimmering() {
        defaultUpdateSubnodesShimmering()
    }
    
    public init() {
        super.init(
            cache: ASPINRemoteImageDownloader.shared(),
            downloader: ASPINRemoteImageDownloader.shared()
        )
    }
}
