//
//  RFLoaderNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 4/9/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFLoaderNode: RFDisplayNode {
    var color: UIColor = .white
    
    open var loaderView: UIActivityIndicatorView? {
        view as? UIActivityIndicatorView
    }
    
    public init(style: UIActivityIndicatorView.Style = .gray, color: UIColor? = nil) {
        if let color = color {
            self.color = color
        }
        
        super.init()
        
        setViewBlock({
            let loaderView = UIActivityIndicatorView(style: style)
            
            loaderView.color = color
            
            loaderView.startAnimating()
            
            return loaderView
        })
    }
    
    public func startAnimating() {
        DispatchQueue.main.async { [weak self] in
            self?.loaderView?.startAnimating()
        }
    }
    
    public func stopAnimating() {
        DispatchQueue.main.async { [weak self] in
            self?.loaderView?.stopAnimating()
        }
    }
}
