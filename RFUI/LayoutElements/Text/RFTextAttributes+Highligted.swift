//
//  RFTextAttributes+Highligted.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 3/7/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension RFTextAttributes {
    var highlighted: RFTextAttributes {
        var attributes = self
        
        if let color = attributes.color {
            attributes.color = color.textHighlighted
        }
        
        return attributes
    }
}
