//
//  RFTextNode+Text.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 5/14/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTextNode: RFTextProtocol {
    open func setAttributes(_ attributes: RFTextAttributes) {
        self.attributes = attributes.attributes
    }

    open func setAttributedText(_ text: String) {
        let attributedText = NSAttributedString(string: text, attributes: attributes)

        self.attributedText = attributedText
    }

    open func appendAttributedText(_ text: String) {
        let attributedText = NSMutableAttributedString()

        let oldAttributedText = self.attributedText ?? NSAttributedString()

        attributedText.append(oldAttributedText)

        let newAttributedText = NSAttributedString(string: text, attributes: attributes)

        attributedText.append(newAttributedText)

        self.attributedText = attributedText
    }

    open func appendAttributedText(_ text: String, _ attributes: RFTextAttributes) {
        let oldAttributes = self.attributes

        let attributedText = NSMutableAttributedString()

        let oldAttributedText = self.attributedText ?? NSAttributedString()

        attributedText.append(oldAttributedText)

        setAttributes(attributes)

        let newAttributedText = NSAttributedString(string: text, attributes: self.attributes)

        attributedText.append(newAttributedText)

        self.attributedText = attributedText

        self.attributes = oldAttributes
    }
}
