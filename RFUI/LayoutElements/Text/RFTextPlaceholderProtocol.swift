//
//  RFTextPlaceholderProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 4/13/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFTextPlaceholderProtocol {
    var placeholderAttributes: [NSAttributedString.Key: Any] { get set }
    
    var attributedPlaceholderText: NSAttributedString? { get set }

    func setPlaceholderAttributes(_ attributes: RFTextAttributes)

    func setPlaceholderAttributedText(_ text: String)

    func appendPlaceholderAttributedText(_ text: String)

    func appendPlaceholderAttributedText(_ text: String, _ attributes: RFTextAttributes)
}
