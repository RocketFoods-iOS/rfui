//
//  RFTitledTextNode+Text.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 17.07.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTitledTextNode: RFTextProtocol {
    open func setAttributes(_ attributes: RFTextAttributes) {
        self.attributes = attributes.attributes

        let typingAttributes = Dictionary(uniqueKeysWithValues:
            self.attributes.lazy.map { ($0.key.rawValue, $0.value) })

        self.typingAttributes = typingAttributes
    }

    open func setAttributedText(_ text: String) {
        let attributedText = NSAttributedString(string: text, attributes: attributes)

        self.attributedText = attributedText
        
        editableTextNodeDidUpdateText(self)
    }

    open func appendAttributedText(_ text: String) {
        let attributedText = NSMutableAttributedString()

        let oldAttributedText = self.attributedText ?? NSAttributedString()

        attributedText.append(oldAttributedText)

        let newAttributedText = NSAttributedString(string: text, attributes: attributes)

        attributedText.append(newAttributedText)

        self.attributedText = attributedText
        
        editableTextNodeDidUpdateText(self)
    }

    open func appendAttributedText(_ text: String, _ attributes: RFTextAttributes) {
        let oldAttributes = self.attributes

        let attributedText = NSMutableAttributedString()

        let oldAttributedText = self.attributedText ?? NSAttributedString()

        attributedText.append(oldAttributedText)

        setAttributes(attributes)

        let newAttributedText = NSAttributedString(string: text, attributes: self.attributes)

        attributedText.append(newAttributedText)

        self.attributedText = attributedText

        self.attributes = oldAttributes
        
        editableTextNodeDidUpdateText(self)
    }
}
