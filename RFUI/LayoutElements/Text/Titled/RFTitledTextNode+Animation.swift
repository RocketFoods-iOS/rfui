//
//  RFTitledTextNode+Animation.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 27.07.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTitledTextNode: RFAnimationProtocol {
    public func animate(with context: ASContextTransitioning) {
        if animationType == .title {
            let isTextEmpty = attributedText?.length ?? 0 == 0

            if !isTextEmpty, !isTitled {
                isTitled = !isTextEmpty

                titleNode.frame = context.initialFrame(for: titleNode)

                titleNode.transform = titleBottomTransform

                UIView.animate(withDuration: touchAnimationDuration, animations: { [weak self] in

                    guard let self = self else { return }

                    self.titleNode.frame = context.finalFrame(for: self.titleNode)

                    self.titleNode.transform = self.titleTopTransform

                }, completion: { [weak self] in

                    guard let self = self else { return }

                    context.completeTransition($0)

                    self.titleNode.transform = self.titleTopTransform

                })
                
                titleNode.setAttributes(RFTextAttributes(attributes))
                
                if let text = placeholderText {
                    let animation = CATransition()
                    
                    animation.timingFunction = CAMediaTimingFunction(name: .default)
                    animation.type = .fade
                    animation.duration = touchAnimationDuration
                    
                    titleNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
                    
                    titleNode.setAttributedText(text)
                    
                    if isRequired {
                        titleNode.appendAttributedText(" *", RFTextAttributes(requiredAttributes))
                    }
                }

            } else if isTextEmpty, isTitled {
                titleNode.frame = context.initialFrame(for: titleNode)

                titleNode.transform = titleTopTransform

                UIView.animate(withDuration: touchAnimationDuration, animations: { [weak self] in

                    guard let self = self else { return }

                    self.titleNode.frame = context.finalFrame(for: self.titleNode)

                    self.titleNode.transform = self.titleBottomTransform

                }, completion: { [weak self] in

                    guard let self = self else { return }

                    context.completeTransition($0)

                    self.isTitled = !isTextEmpty

                    self.titleNode.transform = self.titleBottomTransform

                })
                
                titleNode.setAttributes(RFTextAttributes(placeholderAttributes))
                
                if let text = placeholderText {
                    let animation = CATransition()
                    
                    animation.timingFunction = CAMediaTimingFunction(name: .default)
                    animation.type = .fade
                    animation.duration = touchAnimationDuration
                    
                    titleNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
                    
                    titleNode.setAttributedText(text)
                    
                    if isRequired {
                        titleNode.appendAttributedText(" *", RFTextAttributes(requiredAttributes))
                    }
                }
            }
        }

        animationType = .none
    }
}
