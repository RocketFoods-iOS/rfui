//
//  RFTitledTextNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 09.06.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFTitledTextNode: ASEditableTextNode, RFElementProtocol, RFElementLayoutSpecsProtocol, RFFocusProtocol {
    public typealias RFTitledTextBlock = ((RFTitledTextNode) -> Void)
    
    open var attributes = [NSAttributedString.Key: Any]()

    open var changedText: String?
    
    open var placeholderText: String?
    
    open var placeholderAttributes = [NSAttributedString.Key: Any]()
    
    open var requiredAttributes = [NSAttributedString.Key: Any]()
    
    open var isRequired = false
    
    open var keyboardFocus: (RFMainNode?, RFScrollNode?, (RFElementProtocol & RFElementLayoutSpecsProtocol)?)?

    open var insets = UIEdgeInsets.zero

    var touchAnimationDuration: TimeInterval { 0.23 }

    let titleTopTransform = CATransform3DScale(CATransform3DTranslate(CATransform3DIdentity, 0, -18, 0), 0.83, 0.83, 1)
    let titleBottomTransform = CATransform3DIdentity

    open var titleNode: RFTextNode = {
        let node = RFTextNode()

        node.style.flexShrink = 1

        node.insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        node.anchorPoint = .zero

        node.transform = CATransform3DIdentity

        return node

    }()

    open var bottomLine: RFDisplayNode = {
        let node = RFDisplayNode()

        node.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimension(unit: .points, value: 1)
        )

        node.insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

        node.backgroundColor = .mainLightGray

        return node

    }()

    public override init() {
        super.init()
        
        autocapitalizationType = .sentences
        autocorrectionType = .no
        spellCheckingType = .no
        
        automaticallyManagesSubnodes = true

        delegate = self
    }

    public override init(textKitComponents: ASTextKitComponents, placeholderTextKitComponents: ASTextKitComponents) {
        super.init(textKitComponents: textKitComponents, placeholderTextKitComponents: placeholderTextKitComponents)
    }

    open override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        let verticalStack = RFVerticalStack()

        verticalStack.addElement(titleNode.insetSpec)
        verticalStack.addElement(RFSpacer)
        verticalStack.addElement(bottomLine.insetSpec)

        return verticalStack
    }

    public enum AnimationType {
        case none
        case title
    }

    open var animationType: AnimationType = .none

    open var isTitled = false

    open var multipleLines = false
    
    open var shouldReturnBlock: RFTitledTextBlock?
    open var shouldBeginEditingBlock: RFTitledTextBlock?
    open var didBeginEditingBlock: RFTitledTextBlock?
    open var didUpdateTextBlock: RFTitledTextBlock?
    open var didFinishEditingBlock: RFTitledTextBlock?
    
    open override func animateLayoutTransition(_ context: ASContextTransitioning) {
        animate(with: context)
    }
}
