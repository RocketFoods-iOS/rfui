//
//  RFTitledTextNode+ASEditableTextNodeDelegate.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 17.07.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTitledTextNode: ASEditableTextNodeDelegate {
    open func editableTextNodeShouldBeginEditing(_: ASEditableTextNode) -> Bool {
        shouldBeginEditingBlock?(self)

        selectedRange = NSRange(location: attributedText?.string.count ?? 0, length: 0)
        
        if let (mainNode, scrollNode, node) = keyboardFocus {
            guard let mainNode = mainNode, let scrollNode = scrollNode, let node = node else {
                return true
            }
            
            mainNode.keyboardFocus = (scrollNode, node)
        }

        return true
    }
    
    public func editableTextNodeDidBeginEditing(_: ASEditableTextNode) {
        didBeginEditingBlock?(self)
        
        UIView.animate(withDuration: 0.25) {
            self.bottomLine.backgroundColor = .mainTintLight
        }
    }

    public func editableTextNodeDidFinishEditing(_: ASEditableTextNode) {
        didFinishEditingBlock?(self)
        
        UIView.animate(withDuration: 0.25) {
            self.bottomLine.backgroundColor = .mainLightGray
        }
        
        if let (mainNode, _, _) = keyboardFocus {
            guard let mainNode = mainNode else { return }
            
            mainNode.keyboardFocus = nil
        }
    }

    public func editableTextNodeDidUpdateText(_: ASEditableTextNode) {
        didUpdateTextBlock?(self)
        
        animationType = .title

        transitionLayout(withAnimation: true, shouldMeasureAsync: true, measurementCompletion: nil)
    }

    open func obtainNewText(_ text: String) -> Bool {
        let isNewLine = text.rangeOfCharacter(from: .newlines) != nil

        if isNewLine, !multipleLines {
            var trimmedText = text

            trimmedText.removeAll(where: { $0.isNewline })

            appendAttributedText(trimmedText)

            return false
        }

        return true
    }

    open func editableTextNode(_: ASEditableTextNode,
                               shouldChangeTextIn _: NSRange, replacementText text: String) -> Bool {
        if text.rangeOfCharacter(from: .newlines) != nil {
            shouldReturnBlock?(self)
        }
        
        return obtainNewText(text)
    }
}
