//
//  RFTitledTextNode+TextPlaceholder.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 4/13/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTitledTextNode: RFTextPlaceholderProtocol {
    open func setPlaceholderAttributes(_ attributes: RFTextAttributes) {
        placeholderAttributes = attributes.attributes
        
        if !isTitled {
            titleNode.setAttributes(attributes)
        }
    }
    
    open func setPlaceholderAttributedText(_ text: String) {
        placeholderText = text
        
        if !isTitled {
            titleNode.setAttributedText(text)
            
            if isRequired {
                titleNode.appendAttributedText(" *", RFTextAttributes(requiredAttributes))
            }
        }
    }
    
    open func appendPlaceholderAttributedText(_ text: String) {
        titleNode.appendAttributedText(text)
    }
    
    open func appendPlaceholderAttributedText(_ text: String, _ attributes: RFTextAttributes) {
        titleNode.appendAttributedText(text, attributes)
    }
}
