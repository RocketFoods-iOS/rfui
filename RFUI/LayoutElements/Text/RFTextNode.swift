//
//  RFTextNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFTextNode: ASTextNode, RFElementProtocol, RFElementLayoutSpecsProtocol {
    open var attributes = [NSAttributedString.Key: Any]()
    
    open var changedText: String?

    open var insets = UIEdgeInsets.zero
    
    public override init() {
        super.init()
        
        automaticallyManagesSubnodes = true
    }
}
