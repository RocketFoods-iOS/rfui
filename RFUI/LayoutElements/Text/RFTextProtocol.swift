//
//  RFTextProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit

public protocol RFTextProtocol {
    var attributes: [NSAttributedString.Key: Any] { get set }
    
    var attributedText: NSAttributedString? { get set }
    
    var changedText: String? { get set }

    func setAttributes(_ attributes: RFTextAttributes)

    func setAttributedText(_ text: String)

    func appendAttributedText(_ text: String)

    func appendAttributedText(_ text: String, _ attributes: RFTextAttributes)
}
