//
//  RFEditableTextNode+ASEditableTextNodeDelegate.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 27.07.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFEditableTextNode: ASEditableTextNodeDelegate {
    open func obtainNewText(_ text: String) -> Bool {
        let isNewLine = text.rangeOfCharacter(from: .newlines) != nil

        if isNewLine, !multipleLines {
            var trimmedText = text

            trimmedText.removeAll(where: { $0.isNewline })

            appendAttributedText(trimmedText)

            return false
        }

        return true
    }
    
    open func editableTextNode(_: ASEditableTextNode,
                               shouldChangeTextIn range: NSRange,
                               replacementText text: String) -> Bool {
        if text.rangeOfCharacter(from: .newlines) != nil {
            shouldReturnBlock?(self)
        }
        
        if let shouldChangeTextBlock = shouldChangeTextBlock {
            return shouldChangeTextBlock(self, range, text)
        }
        
        return obtainNewText(text)
    }

    open func editableTextNodeShouldBeginEditing(_: ASEditableTextNode) -> Bool {
        if let shouldBeginEditingBlock = shouldBeginEditingBlock {
            return shouldBeginEditingBlock(self)
        }

        selectedRange = NSRange(location: attributedText?.string.count ?? 0, length: 0)
        
        if let (mainNode, scrollNode, node) = keyboardFocus {
            guard let mainNode = mainNode, let scrollNode = scrollNode, let node = node else {
                return true
            }
            
            mainNode.keyboardFocus = (scrollNode, node)
        }

        return true
    }

    open func editableTextNodeDidBeginEditing(_: ASEditableTextNode) {
        didBeginEditingBlock?(self)
    }

    open func editableTextNodeDidUpdateText(_: ASEditableTextNode) {
        didUpdateTextBlock?(self)
    }

    open func editableTextNodeDidFinishEditing(_: ASEditableTextNode) {
        didFinishEditingBlock?(self)
        
        if let (mainNode, _, _) = keyboardFocus {
            guard let mainNode = mainNode else { return }
            
            mainNode.keyboardFocus = nil
        }
    }

    open func editableTextNodeDidChangeSelection(_: ASEditableTextNode, fromSelectedRange _: NSRange,
                                                 toSelectedRange _: NSRange, dueToEditing _: Bool) {}
}
