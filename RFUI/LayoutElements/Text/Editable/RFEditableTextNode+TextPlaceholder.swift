//
//  RFEditableTextNode+RFTextPlaceholderProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 4/13/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFEditableTextNode: RFTextPlaceholderProtocol {
    open func setPlaceholderAttributes(_ attributes: RFTextAttributes) {
        placeholderAttributes = attributes.attributes
    }
    
    open func setPlaceholderAttributedText(_ text: String) {
        let attributedText = NSAttributedString(string: text, attributes: placeholderAttributes)

        attributedPlaceholderText = attributedText
        
        editableTextNodeDidUpdateText(self)
    }
    
    open func appendPlaceholderAttributedText(_ text: String) {
        let attributedText = NSMutableAttributedString()

        let oldAttributedText = attributedPlaceholderText ?? NSAttributedString()

        attributedText.append(oldAttributedText)

        let newAttributedText = NSAttributedString(string: text, attributes: placeholderAttributes)

        attributedText.append(newAttributedText)

        attributedPlaceholderText = attributedText
    }
    
    open func appendPlaceholderAttributedText(_ text: String, _ attributes: RFTextAttributes) {
        let oldAttributes = placeholderAttributes

        let attributedText = NSMutableAttributedString()

        let oldAttributedText = attributedPlaceholderText ?? NSAttributedString()

        attributedText.append(oldAttributedText)

        setPlaceholderAttributes(attributes)

        let newAttributedText = NSAttributedString(string: text, attributes: placeholderAttributes)

        attributedText.append(newAttributedText)

        attributedPlaceholderText = attributedText

        placeholderAttributes = oldAttributes
    }
}
