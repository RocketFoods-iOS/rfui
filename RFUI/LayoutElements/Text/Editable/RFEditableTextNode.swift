//
//  RFEditableTextNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFEditableTextNode: ASEditableTextNode, RFElementProtocol, RFElementLayoutSpecsProtocol, RFFocusProtocol {
    public typealias RFEditableTextBlock = ((RFEditableTextNode) -> Void)
    public typealias RFEditableTextBoolBlock = ((RFEditableTextNode) -> Bool)
    
    open var attributes = [NSAttributedString.Key: Any]()
    
    open var changedText: String?

    open var placeholderAttributes = [NSAttributedString.Key: Any]()
    
    open var keyboardFocus: (RFMainNode?, RFScrollNode?, (RFElementProtocol & RFElementLayoutSpecsProtocol)?)?
    
    open var isEmailEntry = false {
        didSet {
            keyboardType = .emailAddress
            
            autocapitalizationType = .none
            autocorrectionType = .no
            
            spellCheckingType = .no
        }
    }
    
    open var isPhoneEntry = false {
        didSet {
            phoneNumber = ""
            
            keyboardType = .numberPad

            setAttributedText("+7 ")
            
            appendAttributedText("(999) 999 99 99", RFTextAttributes(placeholderAttributes))

            setCursor(to: 4)
            
            shouldChangeTextBlock = { [weak self] _, range, text in
                guard var number = self?.phoneNumber else { return false }
                
                if let char = text.cString(using: .utf8), char.count > 0 {
                    if strcmp(char, "\\b") == -92, number.count > 0 {
                        number = String(number.dropLast())

                    } else if char[0] >= 48, char[0] <= 57, number.count < 10 {
                        number += text
                    }
                }
                
                self?.phoneNumber = number

                self?.constructText()

                return false
            }
            
            didBeginEditingBlock = { [weak self] node in
                guard var numberCount = self?.phoneNumber?.count else { return }

                if numberCount <= 3 {
                    numberCount += 4

                } else if numberCount > 3, numberCount <= 6 {
                    numberCount += 6

                } else if numberCount > 6, numberCount <= 8 {
                    numberCount += 7

                } else if numberCount > 8, numberCount <= 10 {
                    numberCount += 8
                }

                self?.setCursor(to: numberCount)
            }
        }
    }
    
    open var didFilledPhoneBlock: ((String) -> Void)?
    
    var phoneNumber: String?

    open var insets = UIEdgeInsets.zero

    open var multipleLines = true
    
    open var shouldBeginEditingBlock: RFEditableTextBoolBlock?
    open var shouldReturnBlock: RFEditableTextBlock?
    open var shouldChangeTextBlock: ((RFEditableTextNode, NSRange, String) -> Bool)?
    open var didBeginEditingBlock: RFEditableTextBlock?
    open var didUpdateTextBlock: RFEditableTextBlock?
    open var didFinishEditingBlock: RFEditableTextBlock?
    

    public override init() {
        super.init()
        
        autocapitalizationType = .sentences
        autocorrectionType = .no
        spellCheckingType = .no
        
        automaticallyManagesSubnodes = true

        delegate = self
    }

    public override init(textKitComponents: ASTextKitComponents, placeholderTextKitComponents: ASTextKitComponents) {
        super.init(textKitComponents: textKitComponents, placeholderTextKitComponents: placeholderTextKitComponents)

        delegate = self
    }
    
    open func setCursor(to offset: Int) {
        if let newPosition = textView.position(from: textView.beginningOfDocument, offset: offset) {
            textView.selectedTextRange = textView.textRange(from: newPosition, to: newPosition)
        }
    }
    
    fileprivate func appendPart(by offset: Int, pattern: String, whitespaces: Int, previousCompleted: Bool = true) -> Bool {
        guard let number = phoneNumber else { return false }
        
        let prefix = pattern.count

        let part: String = {
            if previousCompleted {
                return String(number[number.index(number.startIndex, offsetBy: offset) ..< number.endIndex].prefix(prefix))
            }

            return ""

        }()

        let placeholderLength = part.count <= prefix ? prefix - part.count : 0

        appendAttributedText(String(part.prefix(prefix - placeholderLength)), RFTextAttributes(attributes))
        appendAttributedText(String(pattern.suffix(placeholderLength)), RFTextAttributes(placeholderAttributes))

        let startPosition = offset + prefix

        if number.count < startPosition, previousCompleted {
            setCursor(to: startPosition + whitespaces + part.count)
        }

        return placeholderLength == 0
    }

    fileprivate func constructText() {
        guard let number = phoneNumber else { return }
        
        setAttributedText("+7 ")

        appendAttributedText("(", number.count > 0 ? RFTextAttributes(attributes) : RFTextAttributes(placeholderAttributes))
        let codeCompleted = appendPart(by: 0, pattern: "999", whitespaces: 1)
        appendAttributedText(") ", number.count > 0 ? RFTextAttributes(attributes) : RFTextAttributes(placeholderAttributes))

        let firstCompleted = appendPart(by: 3, pattern: "999", whitespaces: 3, previousCompleted: codeCompleted)
        appendAttributedText(" ", RFTextAttributes(placeholderAttributes))

        let secondCompleted = appendPart(by: 6, pattern: "99", whitespaces: 5, previousCompleted: firstCompleted)
        appendAttributedText(" ", RFTextAttributes(placeholderAttributes))

        _ = appendPart(by: 8, pattern: "99", whitespaces: 6, previousCompleted: secondCompleted)

        didFilledPhoneBlock?(number)
    }
}
