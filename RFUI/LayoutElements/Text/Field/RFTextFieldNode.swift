//
//  RFTextFieldNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/24/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RFAssets

open class RFTextFieldNode: RFActionNode, ASEditableTextNodeDelegate, RFFocusProtocol {
    public typealias RFEditableTextBlock = ((RFEditableTextNode) -> Void)
    public typealias RFEditableTextBoolBlock = ((RFEditableTextNode) -> Bool)
    
    open var shouldBeginEditingBlock: RFEditableTextBoolBlock? {
        get {
            textNode.shouldBeginEditingBlock
        }
        
        set {
            textNode.shouldBeginEditingBlock = newValue
        }
    }
    
    open var shouldReturnBlock: RFEditableTextBlock? {
        get {
            textNode.shouldReturnBlock
        }
        
        set {
            textNode.shouldReturnBlock = newValue
        }
    }
    
    open var didBeginEditingBlock: RFEditableTextBlock? {
       get {
           textNode.didBeginEditingBlock
       }
       
       set {
           textNode.didBeginEditingBlock = newValue
       }
   }
    
    open var didUpdateTextBlock: RFEditableTextBlock?
    
    open var didFinishEditingBlock: RFEditableTextBlock? {
        get {
            textNode.didFinishEditingBlock
        }
        
        set {
            textNode.didFinishEditingBlock = newValue
        }
    }
    
    open var isClearEnabled = true
    
    open var attributes: [NSAttributedString.Key : Any] {
        get {
            textNode.attributes
        }
        
        set {
            textNode.attributes = newValue
        }
    }
    
    open var placeholderAttributes: [NSAttributedString.Key : Any] {
        get {
            textNode.attributes
        }
        
        set {
            textNode.attributes = newValue
        }
    }
    
    open var changedText: String? {
        get {
            textNode.changedText
        }
        
        set {
            textNode.changedText = newValue
        }
    }
    
    open var attributedText: NSAttributedString? {
        get {
            textNode.attributedText
        }
        
        set {
            textNode.attributedText = newValue
        }
    }
    
    open var attributedPlaceholderText: NSAttributedString? {
        get {
            textNode.attributedPlaceholderText
        }
        set {
            textNode.attributedPlaceholderText = newValue
        }
    }
    
    open var iconImage: UIImage? {
        didSet {
            iconNode.image = iconImage
        }
    }
    
    open var autocapitalizationType: UITextAutocapitalizationType {
        get {
            textNode.autocapitalizationType
        }
        
        set {
            textNode.autocapitalizationType = newValue
        }
    }
    
    open var autocorrectionType: UITextAutocorrectionType {
        get {
            textNode.autocorrectionType
        }
        
        set {
            textNode.autocorrectionType = newValue
        }
    }
    
    open var spellCheckingType: UITextSpellCheckingType {
        get {
            textNode.spellCheckingType
        }
        
        set {
            textNode.spellCheckingType = newValue
        }
    }
    
    open var keyboardType: UIKeyboardType {
        get {
            textNode.keyboardType
        }
        
        set {
            textNode.keyboardType = newValue
        }
    }
    
    open var keyboardAppearance: UIKeyboardAppearance {
        get {
            textNode.keyboardAppearance
        }
        
        set {
            textNode.keyboardAppearance = newValue
        }
    }
    
    open var returnKeyType: UIReturnKeyType {
        get {
            textNode.returnKeyType
        }
        
        set {
            textNode.returnKeyType = newValue
        }
    }
    
    open var enablesReturnKeyAutomatically: Bool {
        get {
            textNode.enablesReturnKeyAutomatically
        }
        
        set {
            textNode.enablesReturnKeyAutomatically = newValue
        }
    }
    
    open var keyboardFocus: (RFMainNode?, RFScrollNode?, (RFElementProtocol & RFElementLayoutSpecsProtocol)?)? {
        get {
            textNode.keyboardFocus
        }
        
        set {
            textNode.keyboardFocus = newValue
        }
    }
        
    
    open var textDelegate: UITextViewDelegate? {
        get {
            textNode.textView.delegate
        }
        
        set {
            textNode.textView.delegate = newValue
        }
    }
    
    open var isEmailEntry: Bool {
        get {
            textNode.isEmailEntry
        }
        
        set {
            textNode.isEmailEntry = newValue
        }
    }
    
    open var isPhoneEntry: Bool {
        get {
            textNode.isPhoneEntry
        }
        
        set {
            textNode.isPhoneEntry = newValue
        }
    }
    
    open var didFilledPhoneBlock: ((String) -> Void)? {
        get {
            textNode.didFilledPhoneBlock
        }
        
        set {
            textNode.didFilledPhoneBlock = newValue
        }
    }
    
    let iconNode: RFImageNode = {
        let node = RFImageNode()

        node.size = ASLayoutSize(
            width: ASDimension(unit: .points, value: 14),
            height: ASDimension(unit: .points, value: 14)
        )
        
        node.insets = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 0)

        return node
    }()
    
    var textNode: RFEditableTextNode = {
        var node = RFEditableTextNode()

        node.style.flexGrow = 1

        node.multipleLines = false
        node.isUserInteractionEnabled = false

        node.setAttributes(RFTextAttributes(
            color: .mainDark,
            font: .systemFont(ofSize: 14, weight: .medium),
            letterSpacing: -0.34,
            lineHeight: 18,
            lineBreak: .byTruncatingHead
        ))

        return node
    }()
    
    let clearNode: RFButtonNode = {
        let node = RFButtonNode()

        node.size = ASLayoutSize(
            width: ASDimension(unit: .points, value: 11),
            height: ASDimension(unit: .points, value: 10)
        )
        
        node.insets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 14)
        
        node.slop = UIEdgeInsets(top: -13, left: -12, bottom: -13, right: -14)

        node.normalImage = Asset.Main.textFieldClear.image
        
        node.alpha = 0
        node.isUserInteractionEnabled = false

        return node
    }()

    public override init() {
        super.init()
        
        cornerRadius = 10
        
        normalBackgroundColor = UIColor(248, 248, 248, 1)
        
        textNode.didUpdateTextBlock = { [weak self] in
            guard let self = self else { return }
            
            self.didUpdateTextBlock?($0)
            
            self.transitionLayout(withAnimation: true, shouldMeasureAsync: false)
        }
        
        clearNode.touchUpInsideBlock = { [weak self] _ in
            guard let self = self else { return }
            
            self.textNode.setAttributedText("")
            
            self.textNode.editableTextNodeDidUpdateText(self.textNode)
        }
    }
    
    open override func touchUpInside(_ controlNode: ASControlNode) {
        super.touchUpInside(controlNode)
        
        _ = textNode.becomeFirstResponder()
    }
    
    
    
    open override func canBecomeFirstResponder() -> Bool {
        textNode.canBecomeFirstResponder()
    }
    
    open override func becomeFirstResponder() -> Bool {
        textNode.becomeFirstResponder()
    }
    
    open override func canResignFirstResponder() -> Bool {
        textNode.canResignFirstResponder()
    }
    
    open override func resignFirstResponder() -> Bool {
        textNode.resignFirstResponder()
    }
    
    open override func isFirstResponder() -> Bool {
        textNode.isFirstResponder()
    }
    
    open func setCursor(to offset: Int) {
        textNode.setCursor(to: offset)
    }
    
    open override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        let horizontalStack = RFHorizontalStack(aligned: .none, .center)
        
        textNode.style.spacingBefore = 12
        textNode.style.spacingAfter = 12

        if iconImage != nil {
            horizontalStack.addElement(iconNode.insetSpec)
        } else {
            textNode.style.spacingBefore = 14
        }
        
        horizontalStack.addElement(textNode)
        
        if isClearEnabled {
            horizontalStack.addElement(clearNode.insetSpec)
        }

        return horizontalStack
    }
    
    open override func animateLayoutTransition(_ context: ASContextTransitioning) {
        animate(with: context)
    }
}
