//
//  RFTextFieldNode+Text.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/24/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTextFieldNode: RFTextProtocol {
    open func setAttributes(_ attributes: RFTextAttributes) {
        textNode.setAttributes(attributes)
    }
    
    open func setAttributedText(_ text: String) {
        textNode.setAttributedText(text)
    }
    
    open func appendAttributedText(_ text: String) {
        textNode.appendAttributedText(text)
    }
    
    open func appendAttributedText(_ text: String, _ attributes: RFTextAttributes) {
        textNode.appendAttributedText(text, attributes)
    }
}
