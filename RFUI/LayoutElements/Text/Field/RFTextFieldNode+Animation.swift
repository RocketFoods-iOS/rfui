//
//  RFTextFieldNode+Animation.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 5/29/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTextFieldNode: RFAnimationProtocol {
    public func animate(with context: ASContextTransitioning) {
        changeClearButton(animated: context.isAnimated())
    }
}

fileprivate extension RFTextFieldNode {
    func changeClearButton(animated: Bool) {
        if textNode.attributedText?.string.isEmpty ?? true {
            hideClearButton(animated: animated)
        } else {
            showClearButton(animated: animated)
        }
    }
    
    func showClearButton(animated: Bool) {
        func show() {
            clearNode.alpha = 1
            clearNode.isUserInteractionEnabled = true
        }
        
        if animated {
            UIView.animate(withDuration: touchAnimationDuration) {
                show()
            }
        } else {
            show()
        }
    }
    
    func hideClearButton(animated: Bool) {
        func hide() {
            clearNode.alpha = 0
            clearNode.isUserInteractionEnabled = false
        }
        
        if animated {
            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: {
                hide()
            })
        } else {
            hide()
        }
    }
}
