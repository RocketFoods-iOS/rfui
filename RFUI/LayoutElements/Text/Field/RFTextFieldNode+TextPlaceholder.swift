//
//  RFTextFieldNode+TextPlaceholder.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 4/13/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFTextFieldNode: RFTextPlaceholderProtocol {
    open func setPlaceholderAttributes(_ attributes: RFTextAttributes) {
        textNode.setPlaceholderAttributes(attributes)
    }
    
    open func setPlaceholderAttributedText(_ text: String) {
        textNode.setPlaceholderAttributedText(text)
    }
    
    open func appendPlaceholderAttributedText(_ text: String) {
        textNode.appendAttributedText(text)
    }
    
    open func appendPlaceholderAttributedText(_ text: String, _ attributes: RFTextAttributes) {
        textNode.appendAttributedText(text, attributes)
    }
}
