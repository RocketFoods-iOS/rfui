//
//  RFMapNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/5/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import MapKit

open class RFMapNode: RFDisplayNode {
    public let mapNode: RFDisplayNode
    
    public let mapView = MKMapView()
    
    public override init() {
        mapNode = RFMapNode.mapNode(mapView)
        
        super.init()
    }
}
