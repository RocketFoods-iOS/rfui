//
//  RFMapNode+Elements.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/5/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

extension RFMapNode {
    open override func layoutSpecThatFits(_ constrainedSize: ASSizeRange) -> ASLayoutSpec {
        return mapNode.centerSpec
    }
}
