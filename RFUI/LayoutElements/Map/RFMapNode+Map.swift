//
//  RFMapNode+Map.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 2/5/20.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import MapKit

extension RFMapNode {
    class func mapNode(_ mapView: MKMapView) -> RFDisplayNode {
        let node = RFDisplayNode(viewBlock: { mapView }, didLoad: { node in
            guard let mapView = node.view as? MKMapView else { return }

            mapView.showsUserLocation = true
            mapView.showsCompass = true
        })

        node.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimension(unit: .fraction, value: 1)
        )

        return node
    }
    
    open func setRegion(with location: CLLocation, regionRadius: CLLocationDistance = 200, animated: Bool) {
        let coordinateRegion = MKCoordinateRegion(
            center: location.coordinate,
            latitudinalMeters: regionRadius * 2.0,
            longitudinalMeters: regionRadius * 2.0
        )

        mapView.setRegion(coordinateRegion, animated: animated)
    }
    
    open func setUserLocationRegion(animated: Bool) {
        guard let userLocation = mapView.userLocation.location else { return }
        
        setRegion(with: userLocation, animated: animated)
    }
    
    open func zoomIn() {
        var region: MKCoordinateRegion = mapView.region

        region.span.latitudeDelta /= 2.0
        region.span.longitudeDelta /= 2.0

        mapView.setRegion(region, animated: true)
    }
    
    open func zoomOut() {
        var region = mapView.region

        region.span.latitudeDelta = min(region.span.latitudeDelta * 2.0, 180.0)
        region.span.longitudeDelta = min(region.span.longitudeDelta * 2.0, 180.0)

        mapView.setRegion(region, animated: true)
    }
}
