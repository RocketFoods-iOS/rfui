//
//  RFElementProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFElementProtocol {
    var frame: CGRect { get set }
    
    var slop: UIEdgeInsets { get set }
    
    var shadow: RFShadow { get set }

    var border: RFBorder { get set }
    
    var fullCalculatedSize: CGSize { get }
    
    func convert(_ point: CGPoint, to node: ASDisplayNode?) -> CGPoint
}

public extension RFElementProtocol where Self: ASDisplayNode {
    var slop: UIEdgeInsets {
        get {
            return hitTestSlop
        }
        
        set {
            hitTestSlop = newValue
        }
    }

    var shadow: RFShadow {
        get {
            let shadowColor = UIColor(cgColor: self.shadowColor ?? UIColor.clear.cgColor)

            return RFShadow(color: shadowColor, radius: shadowRadius, opacity: shadowOpacity, offset: shadowOffset)
        }

        set {
            clipsToBounds = false

            shadowColor = newValue.color.cgColor
            shadowRadius = newValue.radius
            shadowOpacity = newValue.opacity
            shadowOffset = newValue.offset
        }
    }

    var border: RFBorder {
        get {
            RFBorder(
                color: borderColor?.uiColor ?? .clear,
                width: borderWidth,
                radius: cornerRadius,
                roundingType: RFBorder.RoundingType(cornerRoundingType),
                corners: RFBorder.Corners(maskedCorners)
            )
        }

        set {
            borderColor = newValue.color.cgColor
            borderWidth = newValue.width
            cornerRadius = newValue.radius
            cornerRoundingType = newValue.roundingType.cornerRoundingType
            maskedCorners = newValue.corners.maskedCorners
            
            DispatchQueue.main.async { [weak self] in
                if newValue.roundingType == .defaultSlowCALayer {
                    self?.layer.maskedCorners = newValue.corners.maskedCorners
                }
            }
        }
    }
}

public extension RFElementProtocol where Self: ASDisplayNode & RFElementLayoutSpecsProtocol {
    var fullCalculatedSize: CGSize {
        CGSize(
            width: insets.left + calculatedSize.width + insets.right,
            height: insets.top + calculatedSize.height + insets.bottom
        )
    }
}
