//
//  RFButtonNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RFExtensions

open class RFButtonNode: RFActionNode {
    public typealias RFButtonActionBlock = ((RFActionNode) -> Void)
    
    /// - Default *Rocket Foods* styled button
    
    open var isDefaultStyled: Bool = false {
        didSet {
            guard isDefaultStyled else { return }
            
            normalTextAttributes = RFTextAttributes(
                color: .white,
                font: .systemFont(ofSize: 16, weight: .semibold),
                letterSpacing: 0.04,
                alignment: .center
            )
            
            normalBackgroundColor = .mainTintLight
            normalBorder = RFBorder(radius: 10)
        }
    }
    
    // MARK: - Interface States

    open override var isHighlighted: Bool {
        willSet {
            if isHighlighted != newValue, isEnabled {
                if newValue {
                    setHighlightedState()
                } else {
                    setNormalState()
                }
            }
        }
    }

    open override var isEnabled: Bool {
        willSet {
            if isEnabled != newValue {
                if !newValue {
                    setDisabledState()
                } else if isHighlighted {
                    setHighlightedState()
                } else {
                    setNormalState()
                }
            }
        }
    }
    
    open var isLoading: Bool = false {
        willSet {
            if isLoading != newValue {
                setLoadingState(newValue)
            }
        }
    }
    
    // MARK: - Text

    internal var titleNode: RFTextNode = {
        let node = RFTextNode()

        node.style.flexShrink = 1.0

        return node
    }()
    
    open var titleAlignment = (ASHorizontalAlignment.middle, ASVerticalAlignment.center)
    
    open var normalTextAttributes: RFTextAttributes?
    open var disabledTextAttributes: RFTextAttributes?
    open var highlightedTextAttributes: RFTextAttributes?
    
    open var normalText: String? {
        didSet {
            if isEnabled, !isHighlighted {
                if let normalTextAttributes = normalTextAttributes {
                    titleNode.setAttributes(normalTextAttributes)
                }
                
                if let normalText = normalText {
                    titleNode.setAttributedText(normalText)
                }
            }
        }
    }
    
    open var disabledText: String? {
        didSet {
            if !isEnabled {
                if let disabledTextAttributes = disabledTextAttributes {
                    titleNode.setAttributes(disabledTextAttributes)
                }
                
                if let disabledText = disabledText {
                    titleNode.setAttributedText(disabledText)
                }
            }
        }
    }
    
    open var highlightedText: String? {
        didSet {
            if isEnabled, isHighlighted {
                if let highlightedTextAttributes = highlightedTextAttributes {
                    titleNode.setAttributes(highlightedTextAttributes)
                } else if let normalTextAttributes = normalTextAttributes {
                    titleNode.setAttributes(normalTextAttributes)
                }
                
                if let highlightedText = highlightedText {
                    titleNode.setAttributedText(highlightedText)
                }
            }
        }
    }
    
    // MARK: - Image

    internal var imageNode: RFImageNode = {
        let node = RFImageNode()

        return node
    }()
    
    open var imageAlignment = (ASHorizontalAlignment.middle, ASVerticalAlignment.center)
    
    open var normalImage: UIImage? {
        didSet {
            if isEnabled, !isHighlighted {
                imageNode.image = normalImage
            }
        }
    }
    
    open var disabledImage: UIImage? {
        didSet {
            if !isEnabled {
                imageNode.image = disabledImage
            }
        }
    }
    
    open var highlightedImage: UIImage? {
        didSet {
            if isEnabled, isHighlighted {
                imageNode.image = highlightedImage
            }
        }
    }
    
    open var imageContentMode: UIView.ContentMode = .center {
        didSet {
            imageNode.contentMode = imageContentMode
        }
    }
    
    // MARK: - Loader
    
    open var loaderNode: RFLoaderNode?
    
    open var loaderAlignment = (ASHorizontalAlignment.middle, ASVerticalAlignment.center)
    
    open var loaderColor: UIColor?

    public override init() {
        super.init()
        
        backgroundColor = normalBackgroundColor
        
        if let normalBorder = normalBorder {
            border = normalBorder
        }
        
        imageNode.image =  normalImage
    }
    
    open override func didLoad() {
        super.didLoad()
        
        updateShimmering()
        
        loaderNode = {
            let node = RFLoaderNode(style: .white, color: loaderColor)
            
            node.stopAnimating()
            
            return node
        }()
        
        setNeedsLayout()
        layoutIfNeeded()
    }
    
    // MARK: - Layout

    open override func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        let imageStack = RFHorizontalStack(aligned: imageAlignment.0, imageAlignment.1)
        
        imageStack.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimension(unit: .fraction, value: 1)
        )
        
        imageStack.addElement(imageNode)
        
        let titleStack = RFHorizontalStack(aligned: titleAlignment.0, titleAlignment.1)
        
        titleStack.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimension(unit: .fraction, value: 1)
        )
        
        titleStack.addElement(titleNode)
        
        let imageTitleSpec = ASBackgroundLayoutSpec(child: imageStack, background: titleStack)
        
        guard let loaderNode = loaderNode else {
            return ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: .minimumXY, child: imageTitleSpec)
        }
        
        let loaderStack = RFHorizontalStack(aligned: loaderAlignment.0, loaderAlignment.1)
        
        loaderStack.size = ASLayoutSize(
            width: ASDimension(unit: .fraction, value: 1),
            height: ASDimension(unit: .fraction, value: 1)
        )
        
        loaderStack.addElement(loaderNode)
        
        let imageTitleLoaderSpec = ASBackgroundLayoutSpec(child: imageTitleSpec, background: loaderStack)

        return ASCenterLayoutSpec(centeringOptions: .XY, sizingOptions: .minimumXY, child: imageTitleLoaderSpec)
    }
}

// MARK: - State Workers

fileprivate extension RFButtonNode {
    func setNormalState() {
        if let normalBackgroundColor = normalBackgroundColor {
            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.backgroundColor = normalBackgroundColor
            })
        }
        
        if let normalBorder = normalBorder {
            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.border = normalBorder
            })
        }
        
        if let normalTextAttributes = normalTextAttributes {
            titleNode.setAttributes(normalTextAttributes)
        }
        
        if let normalText = normalText {
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = touchAnimationDuration
            
            titleNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            
            titleNode.setAttributedText(normalText)
        }
        
        if let normalImage = normalImage {
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = touchAnimationDuration
            
            imageNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            
            
            imageNode.image = normalImage
        }
    }
    
    func setHighlightedState() {
        if let highlightedBackgroundColor = highlightedBackgroundColor
            ?? normalBackgroundColor?.highlighted {

            UIView.animate(withDuration: 0, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.backgroundColor = highlightedBackgroundColor
            })
        }
        
        if let highlightedBorder = highlightedBorder
            ?? normalBorder?.highlighted {

            UIView.animate(withDuration: 0, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.border = highlightedBorder
            })
        }
        
        if let highlightedTextAttributes = highlightedTextAttributes
            ?? normalTextAttributes?.highlighted {
            
            titleNode.setAttributes(highlightedTextAttributes)
        }
        
        if let highlightedText = highlightedText
            ?? normalText {
            
            titleNode.setAttributedText(highlightedText)
        }
        
        if let highlightedImage = highlightedImage
            ?? normalImage {
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = touchAnimationDuration
            
            imageNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            
            imageNode.image = highlightedImage
        }
    }
    
    func setDisabledState() {
        if let disabledBackgroundColor = disabledBackgroundColor
            ?? normalBackgroundColor?.disabled {

            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.backgroundColor = disabledBackgroundColor
            })
        }
        
        if let disabledBorder = disabledBorder
            ?? normalBorder?.disabled {

            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.border = disabledBorder
            })
        }
        
        if let disabledTextAttributes = disabledTextAttributes
            ?? normalTextAttributes?.disabled {
            
            titleNode.setAttributes(disabledTextAttributes)
        }
        
        if let disabledText = disabledText
            ?? normalText {
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = touchAnimationDuration
            
            DispatchQueue.main.async { [weak self] in
                self?.titleNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            }
            
            titleNode.setAttributedText(disabledText)
        }
        
        if let disabledImage = disabledImage
            ?? normalImage {
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = touchAnimationDuration
            
            imageNode.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            
            imageNode.image = disabledImage
        }
    }
    
    func setLoadingState(_ isLoading: Bool) {
        isUserInteractionEnabled = !isLoading
        
        DispatchQueue.main.async { [weak self] in
            guard let self = self else { return }
            
            UIView.animate(withDuration: self.touchAnimationDuration) {
                self.titleNode.alpha = isLoading ? 0 : 1
                self.imageNode.alpha = isLoading ? 0 : 1
            }
            
            let animation = CATransition()
            
            animation.timingFunction = CAMediaTimingFunction(name: .default)
            animation.type = .fade
            animation.duration = self.touchAnimationDuration
            
            self.loaderNode?.layer.add(animation, forKey: CATransitionType.fade.rawValue)
            
            if isLoading {
                self.loaderNode?.startAnimating()
            } else {
                self.loaderNode?.stopAnimating()
            }
        }
    }
}
