//
//  RFActionNode.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 25.07.2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit
import RFExtensions

open class RFActionNode: ASControlNode, RFButtonDelegate, RFElementProtocol, RFElementLayoutSpecsProtocol, RFShimmerProtocol {
    public typealias RFButtonActionBlock = ((RFActionNode) -> Void)
    
    open var insets = UIEdgeInsets.zero
    
    // MARK: - Shimmer
    
    open var isShimmering = false {
        didSet {
            isUserInteractionEnabled = !isShimmering
        }
    }
    
    open lazy var shimmerAnimation: CAKeyframeAnimation = {
        defaultShimmerAnimation
    }()
    
    open lazy var shimmerLayer: CAGradientLayer = {
        defaultShimmerLayer
    }()
    
    open var shimmeringNode: ASDisplayNode { self }
    
    open func updateShimmering() {
        defaultUpdateShimmering()
    }
    
    open func updateSubnodesShimmering() {
        defaultUpdateSubnodesShimmering()
    }
    
    // MARK: - Background Color
    
    /// - You should use this property instead of **backgroundColor**

    open var normalBackgroundColor: UIColor? {
        didSet {
            if isEnabled, !isHighlighted {
                backgroundColor = normalBackgroundColor
            }
        }
    }
    
    open var disabledBackgroundColor: UIColor? {
        didSet {
            if !isEnabled {
                backgroundColor = disabledBackgroundColor
            }
        }
    }
    
    open var highlightedBackgroundColor: UIColor? {
        didSet {
            if isEnabled, isHighlighted {
                backgroundColor = highlightedBackgroundColor
            }
        }
    }
    
    // MARK: - Border
    
    /// - You should use this property instead of **border**

    open var normalBorder: RFBorder? {
        didSet {
            if isEnabled, !isHighlighted, let normalBorder = normalBorder {
                border = normalBorder
            }
        }
    }
    
    open var disabledBorder: RFBorder? {
        didSet {
            if !isEnabled, let disabledBorder = disabledBorder {
                border = disabledBorder
            }
        }
    }
    
    open var highlightedBorder: RFBorder? {
        didSet {
            if isEnabled, isHighlighted, let highlightedBorder = highlightedBorder {
                border = highlightedBorder
            }
        }
    }
    
    // MARK: - Interface States

    open override var isHighlighted: Bool {
        willSet {
            if isHighlighted != newValue, isEnabled {
                if newValue {
                    setHighlightedState()
                } else {
                    setNormalState()
                }
            }
        }
    }

    open override var isEnabled: Bool {
        willSet {
            if isEnabled != newValue {
                if !newValue {
                    setDisabledState()
                } else if isHighlighted {
                    setHighlightedState()
                } else {
                    setNormalState()
                }
            }
        }
    }

    public override init() {
        super.init()
        
        automaticallyManagesSubnodes = true
        
        backgroundColor = normalBackgroundColor
        
        if let normalBorder = normalBorder {
            border = normalBorder
        }
        
        addActions()
    }
    
    open override func didLoad() {
        super.didLoad()
        
        updateShimmering()
    }
    
    // MARK: - Touch Actions

    open func addActions() {
        addAction(for: .touchDown) { self.touchDown(self) }
        addAction(for: .touchDownRepeat) { self.touchDownRepeat(self) }
        addAction(for: .touchDragInside) { self.touchDragInside(self) }
        addAction(for: .touchDragOutside) { self.touchDragOutside(self) }
        addAction(for: .touchUpInside) { self.touchUpInside(self) }
        addAction(for: .touchUpOutside) { self.touchUpOutside(self) }
        addAction(for: .touchCancel) { self.touchCancel(self) }
        addAction(for: .valueChanged) { self.valueChanged(self) }
    }

    open var touchAnimationDuration: TimeInterval { 0.23 }
    
    open var touchAnimationOptions: UIView.AnimationOptions {
        [.beginFromCurrentState, .allowUserInteraction]
    }

    open var touchDownBlock: RFButtonActionBlock?

    open var touchDownDisabledBlock: RFButtonActionBlock?

    open func touchDown(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchDownDisabledBlock?(self)
            
            return
        }
        
        isHighlighted = true

        touchDownBlock?(self)
    }

    open var touchDownRepeatBlock: RFButtonActionBlock?

    open var touchDownRepeatDisabledBlock: RFButtonActionBlock?

    open func touchDownRepeat(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchDownRepeatDisabledBlock?(self)
            
            return
        }
        
        isHighlighted = true

        touchDownRepeatBlock?(self)
    }

    open var touchDragInsideBlock: RFButtonActionBlock?

    open var touchDragInsideDisabledBlock: RFButtonActionBlock?

    open func touchDragInside(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchDragInsideDisabledBlock?(self)
            
            return
        }

        touchDragInsideBlock?(self)
    }

    open var touchDragOutsideBlock: RFButtonActionBlock?

    open var touchDragOutsideDisabledBlock: RFButtonActionBlock?

    open func touchDragOutside(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchDragOutsideDisabledBlock?(self)
            
            return
        }

        touchDragOutsideBlock?(self)
    }

    open var touchUpInsideBlock: RFButtonActionBlock?

    open var touchUpInsideDisabledBlock: RFButtonActionBlock?

    open func touchUpInside(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchUpInsideDisabledBlock?(self)
            
            return
        }
        
        isHighlighted = false

        touchUpInsideBlock?(self)
    }

    open var touchUpOutsideBlock: RFButtonActionBlock?

    open var touchUpOutsideDisabledBlock: RFButtonActionBlock?

    open func touchUpOutside(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchUpOutsideDisabledBlock?(self)
            
            return
        }
        
        isHighlighted = false

        touchUpOutsideBlock?(self)
    }

    open var touchCancelBlock: RFButtonActionBlock?

    open var touchCancelDisabledBlock: RFButtonActionBlock?

    open func touchCancel(_ controlNode: ASControlNode) {
        guard isEnabled else {
            touchCancelDisabledBlock?(self)
            
            return
        }
        
        isHighlighted = false

        touchCancelBlock?(self)
    }

    open var valueChangedBlock: RFButtonActionBlock?

    open func valueChanged(_ controlNode: ASControlNode) {
        valueChangedBlock?(self)
    }
}

fileprivate extension RFActionNode {
    func setNormalState() {
        if let normalBackgroundColor = normalBackgroundColor {
            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.backgroundColor = normalBackgroundColor
            })
        }
        
        if let normalBorder = normalBorder {
            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.border = normalBorder
            })
        }
    }
    
    func setHighlightedState() {
        if let highlightedBackgroundColor = highlightedBackgroundColor
            ?? normalBackgroundColor?.highlighted {

            UIView.animate(withDuration: 0, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.backgroundColor = highlightedBackgroundColor
            })
        }
        
        if let highlightedBorder = highlightedBorder
            ?? normalBorder?.highlighted {

            UIView.animate(withDuration: 0, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.border = highlightedBorder
            })
        }
    }
    
    func setDisabledState() {
        if let disabledBackgroundColor = disabledBackgroundColor
            ?? normalBackgroundColor?.disabled {

            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.backgroundColor = disabledBackgroundColor
            })
        }
        
        if let disabledBorder = disabledBorder
            ?? normalBorder?.disabled {

            UIView.animate(withDuration: touchAnimationDuration, delay: 0,
                           options: touchAnimationOptions, animations: { [weak self] in
               self?.border = disabledBorder
            })
        }
    }
}
