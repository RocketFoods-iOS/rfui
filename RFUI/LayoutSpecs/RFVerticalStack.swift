//
//  RFVerticalStack.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

open class RFVerticalStack: ASStackLayoutSpec, RFElementLayoutSpecsProtocol {
    open var insets = UIEdgeInsets.zero

    open var insetSpec: ASInsetLayoutSpec {
        let insetSpec = ASInsetLayoutSpec(insets: insets, child: self)
        
        insetSpec.style.flexGrow = style.flexGrow
        insetSpec.style.flexShrink = style.flexShrink
        
        return insetSpec
    }

    open var centerSpec: ASCenterLayoutSpec {
        let centerSpec = ASCenterLayoutSpec(
            horizontalPosition: .center,
            verticalPosition: .center,
            sizingOption: .minimumSize,
            child: self
        )
        
        insetSpec.style.flexGrow = style.flexGrow
        insetSpec.style.flexShrink = style.flexShrink
        
        return centerSpec
    }
    
    open var squareSpec: ASRatioLayoutSpec {
        let squareSpec = ASRatioLayoutSpec(ratio: 1, child: self)
        
        squareSpec.style.flexGrow = style.flexGrow
        squareSpec.style.flexShrink = style.flexShrink
        
        return squareSpec
    }

    open var size: ASLayoutSize {
        get {
            return style.preferredLayoutSize
        }

        set {
            style.preferredLayoutSize = newValue
        }
    }

    open func addElement(_ element: ASLayoutElement) {
        var children = self.children ?? [ASLayoutElement]()

        children.append(element)

        self.children = children
    }

    public override init() {
        super.init()

        direction = .vertical
    }

    public init(aligned horizontalAlignment: ASHorizontalAlignment, _ verticalAlignment: ASVerticalAlignment) {
        super.init()

        direction = .vertical

        self.horizontalAlignment = horizontalAlignment
        self.verticalAlignment = verticalAlignment
    }
}
