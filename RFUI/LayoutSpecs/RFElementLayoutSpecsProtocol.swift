//
//  RFElementLayoutSpecsProtocol.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 01/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public protocol RFElementLayoutSpecsProtocol {
    var size: ASLayoutSize { get set }

    var insets: UIEdgeInsets { get set }
    
    var insetSpec: ASInsetLayoutSpec { get }

    var centerSpec: ASCenterLayoutSpec { get }
    
    var squareSpec: ASRatioLayoutSpec { get }
    
    var wrapperSpec: ASWrapperLayoutSpec { get }
}

public extension RFElementLayoutSpecsProtocol where Self: ASLayoutElement {
    var size: ASLayoutSize {
        get {
            return style.preferredLayoutSize
        }

        set {
            style.preferredLayoutSize = newValue
        }
    }

    var insetSpec: ASInsetLayoutSpec {
        let insetSpec = ASInsetLayoutSpec(insets: insets, child: self)
        
        insetSpec.style.flexGrow = style.flexGrow
        insetSpec.style.flexShrink = style.flexShrink
        
        return insetSpec
    }

    var centerSpec: ASCenterLayoutSpec {
        let centerSpec = ASCenterLayoutSpec(
            horizontalPosition: .center,
            verticalPosition: .center,
            sizingOption: .minimumSize,
            child: self
        )
        
        centerSpec.style.flexGrow = style.flexGrow
        centerSpec.style.flexShrink = style.flexShrink
        
        return centerSpec
    }
    
    var squareSpec: ASRatioLayoutSpec {
        let squareSpec = ASRatioLayoutSpec(ratio: 1, child: self)
        
        squareSpec.style.flexGrow = style.flexGrow
        squareSpec.style.flexShrink = style.flexShrink
        
        return squareSpec
    }
    
    var wrapperSpec: ASWrapperLayoutSpec {
        ASWrapperLayoutSpec(layoutElement: self)
    }
}
