//
//  ASInsetLayoutSpec.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 14.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public extension ASInsetLayoutSpec {
    var centerSpec: ASCenterLayoutSpec {
        let centerSpec = ASCenterLayoutSpec(
            horizontalPosition: .center,
            verticalPosition: .center,
            sizingOption: .minimumSize,
            child: self
        )
        
        centerSpec.style.flexGrow = style.flexGrow
        centerSpec.style.flexShrink = style.flexShrink
        
        return centerSpec
    }
}
