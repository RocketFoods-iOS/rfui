//
//  RFBorder.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 04/05/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit
import AsyncDisplayKit

public struct RFBorder {
    public enum RoundingType {
        case defaultSlowCALayer
        case precomposited
        case clipping
        
        public init(_ cornerRoundingType: ASCornerRoundingType) {
            self = {
                switch cornerRoundingType {
                case .defaultSlowCALayer:
                    return .defaultSlowCALayer
                case .precomposited:
                    return .precomposited
                case .clipping:
                    return .clipping
                @unknown default:
                    return .defaultSlowCALayer
                }
            }()
        }
        
        var cornerRoundingType: ASCornerRoundingType {
            switch self {
            case .defaultSlowCALayer:
                return .defaultSlowCALayer
            case .precomposited:
                return .precomposited
            case .clipping:
                return .clipping
            }
        }
    }
    
    public struct Corners: OptionSet {
        public var rawValue: UInt
        
        public static var leftTop = Corners(rawValue: 1 << 0)
        public static var rightTop = Corners(rawValue: 1 << 1)
        public static var leftBottom = Corners(rawValue: 1 << 2)
        public static var rightBottom = Corners(rawValue: 1 << 3)
        
        public static var allLeft = Corners([.leftTop, .leftBottom])
        public static var allRight = Corners([.rightTop, .rightBottom])
        public static var allTop = Corners([.leftTop, .rightTop])
        public static var allBottom = Corners([.leftBottom, .rightBottom])
        
        public static var all = Corners([.leftBottom, .leftTop, .rightTop, .rightBottom])
        
        public static var none = Corners([])
        
        public init(rawValue: UInt) {
            self.rawValue = rawValue
        }
        
        public init(_ maskedCorners: CACornerMask) {
            self.rawValue = maskedCorners.rawValue
        }
        
        public var maskedCorners: CACornerMask {
            CACornerMask(rawValue: rawValue)
        }
    }
    
    public let color: UIColor
    public let width: CGFloat
    public let radius: CGFloat
    public let roundingType: RoundingType
    public let corners: Corners

    public init(color: UIColor = .clear, width: CGFloat = 0, radius: CGFloat = 0,
                roundingType: RoundingType = .defaultSlowCALayer, corners: Corners = .none) {
        self.color = color
        self.width = width
        self.radius = radius
        self.roundingType = roundingType
        self.corners = corners == .none && radius != 0 ? .all : corners
    }

    public static let none = RFBorder()
}
