//
//  RFBorder+Disabled.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 08.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension RFBorder {
    public var disabled: Self {
        Self(color: .mainLightGray, width: width, radius: radius, roundingType: roundingType, corners: corners)
    }
}
