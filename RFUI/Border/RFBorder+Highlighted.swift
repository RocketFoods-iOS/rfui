//
//  RFBorder+Highlighted.swift
//  RFUI
//
//  Created by Nikita Arutyunov on 08.09.2020.
//  Copyright © 2020 Nikita Arutyunov. All rights reserved.
//

import UIKit

extension RFBorder {
    public var highlighted: Self {
        guard let hue = color.hue,
            let saturation = color.saturation,
            var brightness = color.brightness,
            let alpha = color.alpha else { return self }
        
        let factor = CGFloat(0.098)
        
        brightness -= brightness > 0.5 ? factor : -factor
        
        let color = UIColor(hue: hue, saturation: saturation, brightness: brightness, alpha: alpha)
        
        return Self(color: color, width: width, radius: radius, roundingType: roundingType, corners: corners)
    }
}
